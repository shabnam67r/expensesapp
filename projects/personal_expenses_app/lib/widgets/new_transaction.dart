import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../widgets/adaptive_flat_button.dart';

class NewTransaction extends StatefulWidget {
  final Function addTx;
  NewTransaction(this.addTx);

  @override
  State<NewTransaction> createState() => _NewTransactionState();
}

class _NewTransactionState extends State<NewTransaction> {
  final _titleController = TextEditingController();
  DateTime _selectedDate = DateTime.now();

  final _amountController = TextEditingController();
  @override
  void _submitHandler() {
    final enteredTitle = _titleController.text;
    final enteredAmount = double.parse(_amountController.text);

    if (enteredTitle.isEmpty || enteredAmount <= 0 || _selectedDate == null) {
      return;
    }
    widget.addTx(
        _titleController.text,
        double.parse(
          _amountController.text,
        ),
        _selectedDate);
    Navigator.of(context).pop();
  }

  void _presentDatePicker() {
    showDatePicker(
            context: context,
            initialDate: DateTime.now(),
            firstDate: DateTime(2019),
            lastDate: DateTime.now())
        .then((pickedDate) {
      if (pickedDate == null) {
        return;
      }
      setState(() {
        _selectedDate = pickedDate;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Card(
        elevation: 5,
        child: Container(
          margin: EdgeInsets.all(10),
          padding: EdgeInsets.only(
              top: 10,
              left: 10,
              right: 10,
              bottom: MediaQuery.of(context).viewInsets.bottom + 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              TextField(
                decoration: InputDecoration(labelText: 'title'),
                // onChanged: ((value) => titleInput = value),
                controller: _titleController,
                // keyboardType: TextInputType.text,
                onSubmitted: (_) => _submitHandler(),
              ),
              TextField(
                decoration: InputDecoration(labelText: 'amount'),
                // onChanged: (value) => amountInput = value,
                controller: _amountController,
                // keyboardType: TextInputType.number,
                onSubmitted: (_) => _submitHandler(),
              ),
              Container(
                height: 70,
                child: Row(
                  children: [
                    Expanded(
                      child: Text(_selectedDate == DateTime.now()
                          ? 'No date picked'
                          : 'Picked Date: ${DateFormat.yMd().format(_selectedDate)}'),
                    ),
                    AdaptiveFlatBtn('picked a date', _presentDatePicker)
                  ],
                ),
              ),
              ElevatedButton(
                onPressed: _submitHandler,
                child: Text(
                  'Add transactions',
                  style: TextStyle(color: Colors.white),
                ),
                style: ButtonStyle(
                  foregroundColor:
                      MaterialStateProperty.all<Color>(Colors.purple),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
